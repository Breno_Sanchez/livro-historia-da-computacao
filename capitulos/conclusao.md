#	Conclusão

[|Home|](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao)

[<<Anterior](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao/-/blob/master/capitulos/justificativa.md) [Próximo>>](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao/-/blob/master/capitulos/referencia.md)

Devemos estar atentos às novas tecnologias e, principalmente, dispostos à adaptálas no dia-a-dia em sala de aula e em laboratório, para trazer um ganho qualitativo no ensino e, conseqüentemente, preparando melhor nossos alunos para o mercado de trabalho cada vez mais competitivo.

Com a adoção deste tipo de dispositivo para projetos na área de digitais, verificouse que o tempo de desenvolvimento, bem como o de prototipação, foram reduzidos, quando comparados com o desenvolvimento, utilizando Circuitos Integrados convencionais, sem contar o fato de que a alteração do projeto é facilmente executada, em virtude dos dispositivos serem programáveis e regraváveis.

Além deste fato, temos outras facilidades de projeto [FER-98] que tornam a utilização tanto do ambiente quanto dos dispositivos, extremamente interessantes, tanto do ponto de vista didático quanto de projeto.

Outro ponto a ser salientado é que os alunos adquirem novas qualificações, pois, além de toda a formação técnica tradicional, aliaram-se novos conhecimentos decorrentes do uso desta tecnologia e novas oportunidades de colocação profissional devem surgir.

[|Home|](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao)

[<<Anterior](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao/-/blob/master/capitulos/justificativa.md) [Próximo>>](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao/-/blob/master/capitulos/referencia.md)
