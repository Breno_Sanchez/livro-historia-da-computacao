# Surgimento do curso de Engenharia de Computação


[|Home|](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao)

[Próximo>>](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao/-/blob/master/capitulos/justificativa.md)

1-Todo mundo sabe o que faz um engenheiro civil, eletricista ou mecânico. Já o engenheiro de computação, são poucos que conhecem”, afirma o professor Fernando Osório, do Instituto de Ciências Matemáticas e de Computação (ICMC) da USP, em São Carlos. Segundo ele, o mesmo ocorre quando a comparação é feita dentro da computação. “Quando os primeiros cursos de engenharia de computação surgiram, na segunda metade da década de 1990, outros como ciências da computação, tecnologia da informação e sistemas de informação já estavam muito bem estabelecidos.
E isso se torna um problema quando os alunos passam os primeiros anos da graduação sem entender onde irão atuar. Foi assim que, há alguns anos, surgiu uma crise de identidade nos estudantes, principalmente naqueles que estavam na metade do curso. “Nós fomos percebendo que não conhecíamos nossa identidade. Só começamos a entender o que era a engenharia de computação depois do terceiro ou quarto ano, e nesse caminho muita gente desistiu”, explica Tiago Daneluzzi, que está no quinto ano do curso no ICMC. Mas diversas iniciativas estão sendo tomadas, tanto por alunos como pelos professores, para divulgar a importância do engenheiro de computação e seu papel na sociedade.

1.1 Um profissional que integra duas áreas
 Pense na tecnologia ao nosso redor. Nosso celular, o computador de bordo do carro, o decodificador de TV a cabo e até mesmo o controle remoto do ar-condicionado. Vá mais longe e pense nos painéis de um avião, um carro autônomo ou um sistema de irrigação. Praticamente todos os dispositivos que utilizamos são computadores que estão cada vez mais complexos. A expressão-chave nesse assunto é “sistemas embarcados”, que são aparelhos que possuem características eletrônicas próprias, e estão integrados em termos de computação e elétrica.
A engenharia de computação surgiu da união entre essas duas áreas. Mas, na verdade, ela sempre esteve presente. “Como os sistemas não eram tão sofisticados, você ‘desviava o profissional’. Era possível fazer com que um engenheiro eletricista ou cientista da computação se especializasse na área oposta”, afirma o professor Osório. Por isso, a partir do momento em que as universidades perceberam uma integração cada vez mais complexa, os cursos começaram a surgir e crescer.
Por causa dessa integração, o engenheiro de computação se torna único, capaz de lidar com problemas de um setor específico. “Esse profissional coloca a mão na massa, entende tanto do hardware como do software. Ele tem um potencial diferenciado e vai além dos profissionais isolados”, explica a professora Kalinka Castelo Branco, vice-coordenadora do curso, que é oferecido em parceria pelo ICMC e pela Escola de Engenharia de São Carlos (EESEES).

[|Home|](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao)

[Próximo>>](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao/-/blob/master/capitulos/justificativa.md)
