 # Referências:

 
[|Home|](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao)

[<<Anterior](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao/-/blob/master/capitulos/conclusao.md) 

1. Alexandre Wolf / Da Assessoria de Comunicação do ICMC
[ALT-95] Altera Corporation. “MAX+PLUS II – AHDL”. Califórnia-USA: Altera. 1995.

1. [ALT-96] Altera Corporation. “MAX+PLUS II – VHDL”. Califórnia-USA: Altera. 1996.

1. [ALT-97] Altera Corporation. “MAX+PLUS II – Getting Started”. CalifórniaUSA:Altera. 1997.

1. [ALT-98] Altera Corporation. “Digital Library – CDROM”. Califórnia-USA: Altera. February 1998. Pp. 21-27.

1. [CHR-97] Christiansen, Donald. “Electronics Engineers Handbook”. Fourth Edition. New York-USA: IEEE Press. 1997. Pp. 11.55 – 11.65.

1. [FER-98] Ferlin, Edson Pedro & Eleutério, Marco Antonio M., “Vantagens na Elaboração 	de 	Projetos 	Digitais 	Utilizando 	Dispositivos 	Lógicos Programáveis”. Anais. IV Simpósio de Pesquisa e Extensão em Tecnologia. Natal-RN-Brasil: UFRN. 1998. Pp. 97 – 99.

1. [FER-99] Ferlin, Edson Pedro & Eleutério, Marco Antonio M., “Creating and Using Programmable Logic Laboratory in the Computer Engineering Course”, 3rd Conference on Engineering Education – ICEE´99, Ostrava-Praga, Czech Republic.10 a 14 de Agosto de 1999.

1. [OLS-98] Olszewski, P. ”Lógica Programável – Novas Tendências”. Elektor Eletrônica & Microinformática. Lisboa-Portugal: Editado por Ferreira & Bento. Nº 159. Março de 1998. Pp. 25 – 27.

[|Home|](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao)

[<<Anterior](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao/-/blob/master/capitulos/conclusao.md) 