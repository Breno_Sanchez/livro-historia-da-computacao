# Justificativas da ascensão do Curso de Engenharia de Computação

[|Home|](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao)

[<<Anterior](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao/-/blob/master/capitulos/sobre.md) [Próximo>>](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao/-/blob/master/capitulos/conclusao.md)

2-O atual estágio tecnológico contribui para a adoção de novas técnicas e ferramenta no ensino de graduação em Engenharia, principalmente na Engenharia da Computação. Muitas empresas, como a Altera, possibilitam firmar convênio com Instituições de Ensino, fornecendo equipamentos e componentes para fins didáticos. Neste sentido, a Pontifícia Universidade Católica do Paraná firmou convênio com a Altera, através do seu Programa Universitário, onde dispositivos lógicos programáveis (PLDs), bem como todo um Ambiente de desenvolvimento (MAX+PLUS II) [ALT-97] são fornecidos para utilização acadêmica.
A disponibilização desses dispositivos vem de encontro a uma necessidade que havia no curso de Engenharia da Computação, em disciplinas como: Sistemas Digitais, e microprocessadores, onde era necessário capacitar os alunos com novas ferramentas e dispositivos de última geração disponíveis no mercado, aliados às vantagens que a utilização destes proporciona em termos de projeto [FER-98].

2.1 O Laboratório de Lógica Programável
Para dar suporte ao desenvolvimento de projetos com estes dispositivos, foi criado o Laboratório de Lógica Programável, conforme mencionado em [FER-99], que é o responsável pelos recursos, tanto de equipamentos e dispositivos, quanto de pessoal técnico que, dentre outros projetos, também desenvolve usando PLDs e gera material técnico (manuais, documentos) e didático, para servir de fonte de pesquisa para novos projetos com estes dispositivos.
Este material fica disponível no laboratório para uso interno do mesmo, como também para que os alunos o utilizem como referência para seus trabalhos e projetos.
Os conhecimentos adquiridos pelo laboratório também são apresentados para a comunidade de uma maneira geral, através da publicação de artigos em seminário, simpósios e congressos nos quais este tipo de informação/experiência seja de interesse.
O laboratório dispõe de computadores (Pentium) utilizados pelos pesquisadores para projeto e também para uso dos alunos nas aulas/experimentos, todos interligados em rede e rodando o sistema operacional Windows NT, por questão de segurança, utilizando o ambiente de desenvolvimento MAX+PLUS II da Altera para os projetos/experimentos.
O laboratório conta ainda com diversos equipamentos para o desenvolvimento dos projetos:
•	Osciloscópios digitais;
•	Mesas digitais;
•	Multímetros;
•	Gravadores de EPROMs;
•	Apagadores de EPROMs;
 • Fontes de Alimentação.
Para facilitar o desenvolvimento dos projetos/experimentos, utilizam-se os Kits Educacionais – Design Laboratory Package, composto por: UP 1 – Education Board, ByteBlaster Programming Cable e Hardlock para o software MAX+PLUS II, aonde a placa Education Board contém dois dispositivos programáveis (PLDs) que podem ser utilizados em conjunto ou em separado, bem como todo o sistema de alimentação e de gravação dos mesmos já disponibilizados para uso, reduzindo, desta forma, o tempo de desenvolvimento do protótipo.

2.2 O Ambiente de Desenvolvimento MAX+PLUS II™
Este software (ver Figura 1) é uma poderosa ferramenta para o desenvolvimento dos projetos digitais utilizando os dispositivos lógicos programáveis e, em especial, os da família Altera, permitindo que façamos todo o desenvolvimento, desde a fase de projeto até a programação do dispositivo, mesmo este estando já fixado no próprio circuito, passando pela compilação dos diversos módulos, simulação do(s) circuito(s) e efetuando a checagem de alguns parâmetros, tais como tempo de atraso e outros.

Na Figura 2 vemos claramente as três etapas no desenvolvimento de um projeto: Entrada do Projeto, Compilação do Projeto e Verificação e Programação do(s) dispositivo(s).

O MAX + PLUS II pode ser dividido em vários módulos, o que permite ao projetista escolher de que maneira será realizado o seu projeto. Por exemplo, pode-se fazer um projeto escolhendo-se um editor gráfico (no qual há a ligação gráfica dos componentes) ou através de um editor de texto (no qual o projeto é implementado via geração de código de programação, utilizando VHDL (VHSIC (Very High Speed Integrated Circuit) Hardware Description Language) [ALT-96] ou AHDL (Altera Hardware Description Language) [ALT-95]).

![](https://www.researchgate.net/profile/Edson-Ferlin/publication/281207116/figure/fig1/AS:649329593290752@1531823647014/MAX-PLUS-II-Windows-95-interface_Q640.jpg)

Figura 1 – Interface para Windows do MAX+PLUS II

![](https://www.researchgate.net/profile/Edson-Ferlin/publication/281207116/figure/fig2/AS:649329593294848@1531823647083/MAX-PLUS-II-Project-Environment_Q320.jpg)

Figura 2 – Visão geral das etapas de desenvolvimento com o MAX+PLUS II

Desta maneira, percebe-se que o MAX + PLUS II é uma ferramenta extremamente flexível e transparente para o projetista. Um projeto desenvolvido no ambiente MAX+PLUS passa por vários estágios antes de chegar a sua implementação final, começando no módulo de Elaboração do Projeto (Text Design Entry, Wave Form Design Entry, Graphic Design Entry, e outros), passando para o módulo de Compilação (onde há geração do código equivalente, para posterior gravação) e, finalmente, há o módulo de Verificação e Programação (MAX+PLUS Programmer, onde ocorre a finalização das verificações e a gravação do Chip Altera propriamente dito).

A gravação dos dispositivos é do tipo ICR (In-Circuit Reconfigurability) ou ISR (In-System Programmability) [ALT-97] ou seja com o dispositivo no próprio circuito; a durabilidade desta programação vai depender do tipo de elemento de armazenamento (memória) que o dispositivo utiliza para armazenar a programação e este elemento pode ser uma EEPROM (Electrically Erasable Programmable Read-Only Memory) [CHR-97], na qual há um período relativamente grande (em anos) ou uma SRAM (Static Random-Access Memory) [CHR-97], aonde a programação permanece válida, enquanto o dispositivo estiver energizado.

2.3	Os Dispositivos Lógicos Programáveis

Os dispositivos lógicos programáveis podem ser divididos em duas categorias: os programáveis uma única vez e os reprogramáveis. Na primeira categoria entram as PLAs (Programmable Array Logic), nas quais a programação é feita mediante a queima dos fusíveis que fazem a ligação entre os componentes internos. Na segunda, que engloba a maioria dos dispositivos programáveis, entra os PLDs (Programmable Logic Devices), CPLDs (Complex-Programmable Logic Devices) e FPGAs (Field-Programmable Gate Array) que são dispositivos que podem ser reprogramados diversas vezes, permitindo que se reutilize-os em outros projetos ou no mesmo, desde que o projeto tenha sido inicialmente desenvolvido com esta particularidade.

Estes dispositivos são extremamente robustos, como por exemplo o EPF10K250B da Altera, que tem internamente, tipicamente, 250.000 portas lógicas que podem ser utilizadas em conjunto com uma RAM de 40.960 bits, operando a uma freqüência de 166 MHz, encapsulado em um envólucro de 600 pinos [ALT-98].

 Estes tipos de dispositivos têm algumas características peculiares [FER-98] que os diferem dos dispositivos tradicionais, tornando-os atraentes no desenvolvimento de projetos digitais.

2.4 As Formas de Programação

Há duas maneiras de se programar estes dispositivos: utilizando uma descrição textual por meio de linguagens como VHDL e AHDL ou por meio gráfico através da interligação de elementos lógicos, como portas lógicas, flip-flops e outros, utilizando o editor gráfico, ou também importando o arquivo gerado pelo programa OrCAD.

No primeiro caso, utilizando-se de um editor de texto, desenvolve-se o projeto utilizando-se uma ou ambas as linguagens de descrição de hardware que estão disponíveis: a VHDL, que é mais conhecida, e a AHDL, que é uma linguagem proprietária da Altera e que segue a mesma filosofia do VHDL.

No segundo, através de um editor gráfico, interliga-se os diversos elementos lógicos gerando as funções desejadas, como se fosse utilizando um editor de esquemático convencional, tipo OrCAD, Tango e outros.

Utilizando-se uma linguagem de descrição de hardware, tipo VHDL e AHDL, pode-se desenvolver a programação através de um dos três modelos de programação: Estrutural (interligação de componentes lógicos normalmente utilizando-se os elementos lógicos (portas lógicas) e biestáveis (flip-flops)), Fluxo de Dados (onde cada sinal de saída é expresso através das equações booleanas) e de Comportamento (baseada em algoritmos, onde a execução é condicionada por acontecimentos como por exemplo, o aparecimento da borda de subida do sinal de clock) [OLS-98].

2.5 Desenvolvimento de Projetos

O ambiente permite que testemos, de antemão, o funcionamento do projeto em módulos, separadamente, bem como integrados, utilizando-se o recurso forma de ondas (simulação), garantindo um menor tempo de projeto/desenvolvimento, o que é extremamente salutar.

Pode-se também verificar, antes de efetuar a programação do(s) dispositivo(s), os tempos de atraso/propagação dos sinais dentro do próprio componente PLD, checando-se o tempo de resposta do(s) sinal(is), com o definido na especificação do projeto, que poderá ser alterado, para atingir a meta almejada.

Nesse tipo de dispositivo, podemos incorporar qualquer função lógica digital, mesmo usando elementos de armazenamento (Flip-Flops) ou mesmo integrá-lo com outros componentes convencionais de eletrônica, tanto digital quanto analógica, como por exemplo microprocessadores/microcontroladores, memórias em semicondutores (RAM e ROM), conversores A/D e D/A e outros.

Os projetos podem, também, ser desenvolvidos utilizando-se a descrição técnica de componentes comerciais, através do uso de bibliotecas-padrão, juntamente com os circuitos desenvolvidos utilizando-se linguagens de descrição de hardware, como AHDL e

VHDL.

Isto é vantajoso em virtude de haver muitos projetos desenvolvidos utilizando-se VHDL que estão muito bem descritos na literatura, de fácil entendimento devido à característica deste tipo de linguagem ser determinística.

Outro ponto a favor é que o tempo de alteração do projeto é o tempo de enviar a programação do(s) dispositivo(s) pelo cabo que compõem a interface de programação, e que consome alguns segundos apenas.

Podemos utilizar este tipo de recurso como material didático, por exemplo nos projetos digitais na graduação, como citado em [FER-99], que foram desenvolvidos por alunos na disciplina de Sistemas Digitais e no Projeto Final, no curso de Engenharia da Computação, utilizando-se os kits educacionais (Design Laboratory Package), que garantem uma rápida prototipação.

Ao final da compilação, o sistema gera um arquivo de Report, no qual consta a descrição dos componentes, porcentagens de utilização dos recursos e toda a programação dos componentes, permitindo um acompanhamento mais preciso do projeto.

Muitas vezes um único dispositivo pode comportar um projeto inteiro, dependendo do tamanho do mesmo, reduzindo os problemas de ligação física entre eles, ficando limitada apenas aos dispositivos auxiliares que não foram incorporados no PLD. Mesmo que isto não seja possível, ainda é viável minimizá-lo devido a possuirmos ligação apenas entre dispositivos e um número reduzido deles.

[|Home|](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao)

[<<Anterior](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao/-/blob/master/capitulos/sobre.md) [Próximo>>](https://gitlab.com/Breno_Sanchez/livro-historia-da-computacao/-/blob/master/capitulos/conclusao.md)

