
# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Tópicos:
1. [Sobre o curso de Engenharia de Computação](capitulos/sobre.md)

1. [Justificativas da ascensão do Curso de Engenharia de Computação](capitulos/justificativa.md)

1. [Conclusão](capitulos/conclusao.md)

1. [Referências](capitulos/referencia.md)





## Autores:
Esse livro foi escrito por:

| Avatar | Nome | Nome do usuário | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168524/avatar.png?width=400)  | José Luiz Setti | josesetti | [zesetti90@gmail.com](mailto:zesetti90@gmail.com)

| Avatar | Nome | Nome do usuário | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9137029/avatar.png?width=400)  | Sumaia Pedon| sumaia1 | [sumayapedon2019@gmail.com](mailto:sumayapedon2019@gmail.com)

| Avatar | Nome | Nome do usuário | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9220893/avatar.png?width=400)  | Breno Gabriel Barão Sanchez | Breno_Sanchez | [brenosanchez11@gmail.com](mailto:brenosanchez11@gmail.com)

| Avatar | Nome | Nome do usuário | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168531/avatar.png?width=400) | Pedro Augusto Merisio | Merisio | [merisio.office@gmail.com](merisio.office@gmail.com)

